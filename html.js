let userNumber;
userNumber = +prompt ("Write a number", "");

// перевірка умови цілого числа
while (Number.isInteger(userNumber) === false) {
    userNumber = +prompt ("Write a number", "");
}

if (userNumber < 5) {
    alert ("Sorry, no numbers");
}

for (let i = 1; i <= userNumber; i++) {
    if (i % 5 == 0) {
        console.log(i);
    }
}




// Необов'язкове завдання підвищеної складності

let m = +prompt ("Write a first number", "");
let n = +prompt ("Write a second number", "");
while (m > n || Number.isInteger(m) === false || Number.isInteger(n) === false) {
    alert("Error, try again");
  m = +prompt ("Write a first number", "");
  n = +prompt ("Write a second number", "");
}
nextPrime:
for (let i = m; i <= n; i++) {  // Для всех i...
    for (let j = m; j < i; j++) {  // проверить, делится ли число..
        if (i % j == 0) continue nextPrime;  // не подходит, берём следующее
    }
    console.log(i);
}

